@echo off
setlocal enabledelayedexpansion

:: Check if port 8080 is in use
netstat -ano | find "LISTENING" | findstr ":8080"
if %errorlevel%==0 (
    echo Port 8080 is in use
) else (
    PowerShell.exe -ExecutionPolicy Bypass -File "clear-standalone.ps1"
)

:: Wait for user input
pause
