# Clear Standalone
The project contains scripts that clear the WildFly standalone.

The script checks if the `standalone.xml` contains the `<deployments>` tag. If yes, it removes this tag and all child tags from the `standalone.xml`.

The script also deletes the following directories:
- `<wildfly-directory>/standalone/configuration/standalone_xml_history`
- `<wildfly-directory>/standalone/data`
- `<wildfly-directory>/standalone/log`
- `<wildfly-directory>/standalone/tmp`


When running the script, it creates a backup of the current `standalone.xml` file. So if the script destroys the `standalone.xml`, you can replace it with the file located at `<wildfly-directory>/backup`.

## How to Use
To use the script, you have to set the environment variable called `WF_HOME_DIR`. In this variable, you have to set the path to your WildFly home directory.

The script expects that you have the standalone folder in this directory. The script also creates a backup directory in this `WF_HOME_DIR` directory.

You have two possibilities to use this script:
1. Ensure that WildFly is not running and run the PowerShell script `clear-standalone.ps1`.
2. Execute the batch file `clear-standalone.bat`. This batch file will check if port 8080 is active. If yes, it will write a message to the console. If not, it will start the PowerShell script `clear-standalone.ps1`.