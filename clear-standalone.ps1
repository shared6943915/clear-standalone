## Get the value of WF_HOME_DIR variable
function GetWildflyHomeDirectory {
    $envVariable = "WF_HOME_DIR"
    $envValue = [Environment]::GetEnvironmentVariable($envVariable)
    if ($envValue -eq $null) {
        Write-Host "$envVariable is not set"
        exit
    }
    Write-Host "The value of $envVariable is $envValue"
    return $envValue
}

## Create necessary path variables
function CreatePathVariables {
    param (
        [string]$envValue
    )
    $configurationFolderPath = Join-Path $envValue "standalone\configuration"
    $dataFolderPath = Join-Path $envValue "standalone\data"
    $logFolderPath = Join-Path $envValue "standalone\log"
    $tmpFolderPath = Join-Path $envValue "standalone\tmp"
    $standaloneXmlHistoryFolderPath = Join-Path $configurationFolderPath "standalone_xml_history"
    $standaloneXmlFilePath = Join-Path $configurationFolderPath "standalone.xml"

    return $configurationFolderPath, $dataFolderPath, $logFolderPath, $tmpFolderPath, $standaloneXmlHistoryFolderPath, $standaloneXmlFilePath
}

## Function to backup the existing standalone.xml file
function Backup-StandaloneXml {
    param (
        [string]$path
    )

    # Define the source file and backup directory
    $sourceFile = Join-Path $path "standalone\configuration\standalone.xml"
    $backupDir = Join-Path $path "backup"

    # Check if backup directory exists. If not, create it
    if (!(Test-Path -Path $backupDir)) {
        New-Item -ItemType Directory -Path $backupDir | Out-Null
    }

    # Define the destination file path
    $destFile = Join-Path $backupDir "standalone.xml"

    # Copy the standalone.xml file from source to destination, overwrite if it exists
    Copy-Item -Path $sourceFile -Destination $destFile -Force
}

## Function to remove deployments tag from standalone.xml file
function Remove-DeploymentsTag {
    param (
        [string]$filePath
    )

    [xml]$xmlDoc = Get-Content $filePath

    # Check if <deployments> tag exists
    $deploymentsNode = $xmlDoc.SelectSingleNode('//*[local-name()="deployments"]')

    if ($deploymentsNode) {
        # Remove <deployemnts> tag will all child tags
        $deploymentsNode.ParentNode.RemoveChild($deploymentsNode)
        $xmlDoc.Save($filePath)
        Write-Host "Removed <deployments> tag."
    }
    else {
        Write-Host "<deployments> tag does not exist."
    }
}

## Function to remove directory including subdirectiories
function Remove-Directory {
    param (
        [string]$path
    )

    if (Test-Path $path) {
        Remove-Item -Path $path -Recurse -Force
        Write-Host "Directory removed: $path"
    }
    else {
        Write-Host "Directory does not exist: $path"
    }
}

$envValue = GetWildflyHomeDirectory

$configurationFolderPath, $dataFolderPath, $logFolderPath, $tmpFolderPath, $standaloneXmlHistoryFolderPath, $standaloneXmlFilePath = CreatePathVariables $envValue

Backup-StandaloneXml -path $envValue

Remove-DeploymentsTag -filePath $standaloneXmlFilePath

Remove-Directory -path $standaloneXmlHistoryFolderPath
Remove-Directory -path $dataFolderPath
Remove-Directory -path $logFolderPath
Remove-Directory -path $tmpFolderPath