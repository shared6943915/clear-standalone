﻿# Changelog

## [1.0.0] - 2024-04-13
**Features:**
- feat: create script to extend the CHANGELOG.md file 
- feat: create script to clear standalone

**Refactors:**
- refactor: change name of script to extend changelog