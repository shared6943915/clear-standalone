# Prompt the user for the next version
$nextVersion = Read-Host -Prompt "What will be the next version"

# Get commit hash for the last chore: next iteration
$sinceDateTime = git log --pretty=format:"%aI|%s" | Select-String -Pattern "\|chore: next iteration$" | Select-Object -First 1 | ForEach-Object { $_.ToString().Split('|')[0] }

# Get the commit logs since the stop hash
$features = git log --no-merges --pretty=format:"%s" --since=$sinceDateTime | Select-String -Pattern "^feat"
$fixes = git log --no-merges --pretty=format:"%s" --since=$sinceDateTime | Select-String -Pattern "^fix"
$refactors = git log --no-merges --pretty=format:"%s" --since=$sinceDateTime | Select-String -Pattern "^refactor"

# Get the current date in yyyy-mm-dd format
$date = Get-Date -Format "yyyy-MM-dd"

# Construct the new lines to add
$newLine1 = "## [$nextVersion] - $date"
$newLine2 = "**Features:**"
$newLine3 = "**Fixes:**"
$newLine4 = "**Refactors:**"

# Convert commit logs to array of strings and prepend "-" to each line
$featureLines = if($features) { $features | ForEach-Object { "`n- $($_.Line)" } }
$fixLines = if($fixes) { $fixes | ForEach-Object { "`n- $($_.Line)" } }
$refactorLines = if($refactors) { $refactors | ForEach-Object { "`n- $($_.Line)" } }

# Read the current contents of the file into an array
$changelogContent = @(Get-Content -Path .\CHANGELOG.md)

# Initiate the new content with section title and version
$newContent = $changelogContent[0..1] + $newLine1

# Check if there are any features
if($featureLines.Length -gt 0) {
    $newContent += $newLine2 + $featureLines
}

# Check if there are any fixes
if($fixLines.Length -gt 0) {
    $newContent += "", ($newLine3 + $fixLines)
}

# Check if there are any refactors
if($refactorLines.Length -gt 0) {
    $newContent += "", ($newLine4 + $refactorLines)
}

# Append existing content from line 3 onward
$newContent += $changelogContent[2..$($changelogContent.Length - 1)]

# Write the new contents back to the file
$newContent | Out-File -FilePath .\CHANGELOG.md -Encoding utf8